﻿using UnityEngine;
using System.Collections;

public class LoadingScript : MonoBehaviour
{
    private float wheelSpeed = 3;


    void Update()
    {
        this.transform.Rotate(Vector3.back, wheelSpeed);
    }
}
