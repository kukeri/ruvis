﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.StaticData;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    private AudioSource backgroundMusic;

    void Start()
    {
        PlayerPrefs.DeleteAll();
        Screen.orientation = ScreenOrientation.Landscape;
        this.backgroundMusic = this.gameObject.GetComponent<AudioSource>();
        PlayMusic();
    }

    private void PlayMusic()
    {
        if (!SettingsStaticData.isMusicMuted)
        {
            backgroundMusic.Play();
        }
    }

    public void OpenMainMenuScene()
    {
        SceneManager.LoadScene("Menu");
    }

}
