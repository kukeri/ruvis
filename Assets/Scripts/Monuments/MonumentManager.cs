﻿using System.Runtime.CompilerServices;

namespace Assets.Scripts.Questions
{
    using System.Collections.Generic;
    using UnityEngine;

    public class MonumentManager : MonoBehaviour
    {
        public List<Question> questions;

        public bool Locked
        {
            get { return lockIndex == 1; }
            set
            {
                if (value)
                {
                    this.lockIndex = 1;
                }
                else
                {
                    this.lockIndex = 0;
                }

                PlayerPrefs.SetInt(this.gameObject.name, lockIndex);
            }
        }

        public bool Focused;

        private int lockIndex;

        void Awake()
        {

            this.lockIndex = PlayerPrefs.GetInt(this.gameObject.name, 1);

            if (this.Locked)
            {
                this.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                this.gameObject.transform.GetChild(1).gameObject.SetActive(false);  
            }
            else
            {
                this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                this.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            }
        }

    }
}

