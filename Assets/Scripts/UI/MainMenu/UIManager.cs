﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject TimerPanel;


    public void Start()
    {
        Screen.orientation = ScreenOrientation.Landscape;
    }

    public void Update()
    {
        this.CheckForBackClicked();
    }

    private void CheckForBackClicked()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.QuitGame();
        }
    }

    public void PlayScene()
    {
        SceneManager.LoadScene("SubjectSelector");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void OpenSettings()
    {
        SceneManager.LoadScene("Settings");
    }

    public void OpenFacebookPage()
    {
        Application.OpenURL("https://www.facebook.com/RuvisTheGame/");
    }

    public void OpenEmailClient()
    {
        Application.OpenURL("mailto:ruvisthegame@gmail.com");
    }

    public void BackToMap()
    {
        this.TimerPanel.SetActive(false);
    }
}
