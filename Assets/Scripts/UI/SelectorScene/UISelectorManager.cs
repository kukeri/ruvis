﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UISelectorManager : MonoBehaviour
{
    public GameObject classChoisePanel;
    private string subjectChosen;
    private string classChosen;
    public GameObject loadingScreen;

    void Update()
    {
        CheckForBackPressed();
    }

    private void CheckForBackPressed()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackToMainMenu();
        }
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void OpenWantedScene()
    {
        string currScene = string.Format("{0}-{1}", this.subjectChosen, this.classChosen);

        this.StartCoroutine(this.LoadLevelAsync(currScene));
    }

    private IEnumerator LoadLevelAsync(string scene)
    {
        this.loadingScreen.SetActive(true);

        AsyncOperation async = SceneManager.LoadSceneAsync(scene.ToString());

        yield return async;
    }


    public void OpenClassChoisePanel()
    {
        classChoisePanel.SetActive(true);
    }

    public void SetSubjectChosen(string subjectChosen)
    {
        this.subjectChosen = subjectChosen;
    }

    public void SetClassChosen(string classChosen)

    {
        this.classChosen = classChosen;
    }

}
