﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.StaticData;
using Vuforia;

public class FlashLight : MonoBehaviour
{             
    void Update()
    {
        if (SettingsStaticData.flashlightTurnedOn)
        {
            CameraDevice.Instance.SetFlashTorchMode(true);
        }
    }
}
