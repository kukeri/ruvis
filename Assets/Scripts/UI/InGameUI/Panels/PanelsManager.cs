﻿using System.Collections;
using UnityEngine;
using Assets.Scripts.UI.InGameUI.Panels;
using System;

public class PanelsManager : MonoBehaviour
{
    public PanelEntry[] canvases;

    public void SetActiveCanvas(UIPanelsInCavas canvasName)
    {
        foreach (PanelEntry panelEntry in canvases)
        {
            if (panelEntry.name == canvasName.ToString())
            {
              
                panelEntry.correspondingCanvasObject.SetActive(true);

            }
            else
            {
                panelEntry.correspondingCanvasObject.SetActive(false);
            }
        }
    }

    private void WaitForCanvas(PanelEntry panelEntry)
    {
      panelEntry.correspondingCanvasObject.SetActive(true);
    }

    public void ResetCanvases()
    {
        SetActiveCanvas(UIPanelsInCavas.MainPanel);
    }
}
