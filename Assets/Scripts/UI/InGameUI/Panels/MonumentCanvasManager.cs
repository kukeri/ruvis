﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MonumentCanvasManager : MonoBehaviour
{
    public Text monumentInformation;
    public MonumentInformationContainer InformationContainer;

    void OnEnable()
    {
        this.monumentInformation.text = InformationContainer.GetCurrentMonumentInformation();
    }
}
