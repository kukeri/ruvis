﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class PanelEntry
{
    public string name;
    public GameObject correspondingCanvasObject;
}
