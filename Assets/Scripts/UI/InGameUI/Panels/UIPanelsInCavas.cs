﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.UI.InGameUI.Panels
{
    public enum UIPanelsInCavas
    {
        SelectedRegionPanel, MonumentPanel, MainPanel, QuestionsPanel
    }

}
