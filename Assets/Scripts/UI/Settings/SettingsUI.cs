﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.StaticData;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour
{
    public Toggle flashlightCheckBox;
    public Toggle isFXMuted;
    public Toggle isMusicMuted;

    void Start()
    {
        this.flashlightCheckBox.isOn = SettingsStaticData.flashlightTurnedOn;
    }

    void Update()
    {
        CheckForBackPressed();
    }

    private void CheckForBackPressed()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LoadMainMenu();
        }
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void UpdateStateOfFlashLight()
    {
        if (this.flashlightCheckBox.isOn)
        {                                                   
            PlayerPrefs.SetInt("flashlight", 1);
            SettingsStaticData.flashlightTurnedOn = true;
        }
        else
        {
            SettingsStaticData.flashlightTurnedOn = false;
            PlayerPrefs.SetInt("flashlight", 0);
        }
    }
}
