﻿using System;
using System.Collections;
using System.Threading;
using Assets.Scripts.Questions;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class FindQuestion : MonoBehaviour
{
    private Text question;
    private Question currentQuestion;
    public Sprite[] boriProfile;
    public Image bori;
    public Button[] button;

    void OnEnable()
    {
        ChangeInteractble(true);


        this.TakeQuestion();

        this.AddTextToQuestion();

    }

    private void AddTextToQuestion()
    {

        this.question = this.GetComponentInChildren<Text>();
        this.question.text = this.currentQuestion.QuestionName;

        for (int i = 0; i < this.button.Length; i++)
        {
            int currentAnswer = Random.Range(0, this.currentQuestion.Answers.Count);
            this.button[i].GetComponentInChildren<Text>().text = this.currentQuestion.Answers[currentAnswer].answer;
            this.currentQuestion.Answers.RemoveAt(currentAnswer);
        }

    }

    private void TakeQuestion()
    {

        int currQuestionIndex = Random.Range(0, QuestionStatic.CurrentQuestions.Count);

        this.currentQuestion = QuestionStatic.CurrentQuestions[currQuestionIndex];

        QuestionStatic.CurrentQuestions.RemoveAt(currQuestionIndex);
    }


    public void CheckForAnswer(Text text)
    {
        if (text.text == this.currentQuestion.CorrectAnswer)
        {
            this.StartCoroutine(this.WaitForWinSprite());
        }
        else
        {

            this.bori.sprite = this.boriProfile[1];
            this.StartCoroutine(this.WaitForLoseingSprite());
        }

    }

    private IEnumerator WaitForLoseingSprite()
    {
        this.bori.sprite = this.boriProfile[1];
        ChangeInteractble(false);
        yield return new WaitForSeconds(0.7f);

        if (QuestionStatic.CurrentQuestions.Count == 0)
        {
            this.gameObject.transform.parent.gameObject.SetActive(false);
            GameObject clock = QuestionStatic.CurrentManager.transform.GetChild(2).gameObject;
            clock.SetActive(true);
            PlayerPrefs.SetInt("Clock", DateTime.Now.Hour);
        }
        else
        {
            ChangeInteractble(true);
            this.bori.sprite = this.boriProfile[2];
            this.TakeQuestion();
            this.AddTextToQuestion();
        }

    }

    private IEnumerator WaitForWinSprite()
    {
        this.bori.sprite = this.boriProfile[0];
        ChangeInteractble(false);

        yield return new WaitForSeconds(0.5f);
        this.bori.sprite = this.boriProfile[2];
        this.gameObject.transform.parent.gameObject.SetActive(false);
        ChangeInteractble(false);
        GameObject monument = QuestionStatic.CurrentManager.transform.GetChild(1).gameObject;
        GameObject flag = QuestionStatic.CurrentManager.transform.GetChild(0).gameObject;
        PlayerPrefs.SetInt(QuestionStatic.CurrentManager.name, 1);
        flag.SetActive(false);
        monument.SetActive(true);

    }

    void ChangeInteractble(bool isInteractble)
    {

        for (int i = 0; i < this.button.Length; i++)
        {
            this.button[i].GetComponent<Button>().interactable = isInteractble;
        }
    }
}
