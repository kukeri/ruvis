﻿
using System.Collections.Generic;
using Assets.Scripts.Questions;

[System.Serializable]
public class Question
{
    public string QuestionName;
    public string CorrectAnswer;
    public List<Answer> Answers;

}
