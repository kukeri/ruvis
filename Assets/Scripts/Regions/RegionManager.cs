﻿using System.Collections;
using Assets.Scripts.Questions;
using Assets.Scripts.UI.InGameUI.Panels;
using UnityEngine;

public class RegionManager : MonoBehaviour
{
    private const int TimeForScale = 100;

    public GameObject position;

    public PanelsManager UIPanelManager;
    public GameObject[] monuments;
    public GameObject locked;

    private int speedForFocusingMonument;
    private int passedTimeOfScale;
    private bool hasFocusedMonument;
    private bool monumentShouldBeFocused;
    private Vector3 previousPositionOfMonument;

    private float deltaToAddForXScale;
    private float deltaToAddForYScale;
    private float deltaToAddForZScale;

    private Transform monumentToFocus;
    private MonumentManager monumentToFocusManager;

    public void ShowHideMonuments()
    {
        foreach (var monument in monuments)
        {
            GameObject parentObjects = monument.transform.parent.gameObject;
            parentObjects.SetActive(!parentObjects.activeSelf);
        }
    }

    void Start()
    {
        passedTimeOfScale = 0;
        this.speedForFocusingMonument = 6;
        monumentShouldBeFocused = false;
        hasFocusedMonument = false;
    }

    void Update()
    {
        ScaleAndFocusSelectedRegion();
    }

    public void SetOnFocus(Transform transformOfGameObjectToFocus)
    {
        if (!hasFocusedMonument)
        {
            this.previousPositionOfMonument = new Vector3(transformOfGameObjectToFocus.position.x, transformOfGameObjectToFocus.position.y, transformOfGameObjectToFocus.position.z);
            this.DeactivateOtherObjects(transformOfGameObjectToFocus);
            this.TakeNeededDataForFocusing(transformOfGameObjectToFocus);
            StartCoroutine(this.PositionObjectOnFocus());

            hasFocusedMonument = true;
        }
    }

    private IEnumerator PositionObjectOnFocus()
    {
        int currentCycle = 0;
        while (currentCycle < TimeForScale)
        {
            currentCycle++;
            this.monumentToFocus.transform.position = Vector3.MoveTowards(this.monumentToFocus.position,
                                             this.position.transform.localPosition, 2f);
            yield return new WaitForSeconds(0.01f);
        }
    }

    private void DeactivateOtherObjects(Transform transformOfGameObjectToFocus)
    {
        foreach (GameObject gameobject in monuments)
        {
            if (gameobject.transform.parent.gameObject.name != transformOfGameObjectToFocus.parent.gameObject.name)
            {
                gameobject.transform.parent.gameObject.SetActive(false);
            }
        }
    }

    private void TakeNeededDataForFocusing(Transform transformOfGameObjectToFocus)
    {
        this.monumentToFocus = transformOfGameObjectToFocus;
        this.monumentToFocusManager = transformOfGameObjectToFocus.gameObject.GetComponent<MonumentManager>();

        monumentShouldBeFocused = true;
        deltaToAddForXScale = transformOfGameObjectToFocus.localScale.x / (TimeForScale / 6);
        deltaToAddForYScale = transformOfGameObjectToFocus.localScale.y / (TimeForScale / 6);
        deltaToAddForZScale = transformOfGameObjectToFocus.localScale.z / (TimeForScale / 6);
    }

    public void SetOutOfFocus()
    {
        StartCoroutine(PositionObjectOutOfFocus());

        monumentShouldBeFocused = true;

        deltaToAddForXScale *= -1;
        deltaToAddForYScale *= -1;
        deltaToAddForZScale *= -1;

        hasFocusedMonument = false;

        foreach (GameObject gameObject in monuments)
        {
            gameObject.transform.parent.gameObject.SetActive(true);
        }

        UIPanelManager.SetActiveCanvas(UIPanelsInCavas.SelectedRegionPanel);
    }

    private IEnumerator PositionObjectOutOfFocus()
    {
        int currentCycle = 0;
        while (currentCycle < TimeForScale)
        {
            currentCycle++;
            this.monumentToFocus.transform.position = Vector3.MoveTowards(this.monumentToFocus.position,
                             this.monumentToFocus.transform.parent.gameObject.transform.GetChild(0).position,
                             speedForFocusingMonument);
            yield return new WaitForSeconds(0.01f);
        }
    }

    private void ScaleAndFocusSelectedRegion()
    {
        if (monumentShouldBeFocused && passedTimeOfScale < TimeForScale / 2)
        {

            this.monumentToFocus.localScale = new Vector3(
                 monumentToFocus.localScale.x + deltaToAddForXScale,
                 monumentToFocus.localScale.y + deltaToAddForYScale,
                 monumentToFocus.localScale.z + deltaToAddForZScale);

            passedTimeOfScale++;
            return;
        }

        if (monumentShouldBeFocused)
        {
            monumentShouldBeFocused = false;
        }

        passedTimeOfScale = 0;
    }
}
