﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Questions;
using Assets.Scripts.UI.InGameUI.Panels;
using UnityEngine.UI;

public class RegionUserInputManager : MonoBehaviour
{
    public PanelsManager UIPanelsManager;
    public MonumentInformationContainer MonumentInformationContainer;
    public GameObject TimerPanel;
    public Text TimerText;
    private RegionManager regionManager;

    void Start()
    {
        this.regionManager = this.gameObject.GetComponent<RegionManager>();
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 positionOfHit = Input.mousePosition;

            var ray = Camera.main.ScreenPointToRay(new Vector3(positionOfHit.x, positionOfHit.y, positionOfHit.z));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.tag == "Monument")
                {
                    GameObject selectedMonument = hit.transform.gameObject;
                    MonumentManager selectedMonumentManager = selectedMonument.transform.parent.GetComponent<MonumentManager>();

                    if (selectedMonumentManager.Focused)
                    {
                        selectedMonumentManager.Focused = false;
                        regionManager.SetOutOfFocus();
                    }
                    else if (!selectedMonumentManager.Focused)
                    {
                        selectedMonumentManager.Focused = true;
                        this.regionManager.SetOnFocus(selectedMonument.transform);
                        this.MonumentInformationContainer.SetCurrentMonumentInformation(selectedMonument.name);
                        this.UIPanelsManager.SetActiveCanvas(UIPanelsInCavas.MonumentPanel);
                    }

                }

               
                if (hit.collider.gameObject.tag == "Flag")
                {
                    GameObject currentMonument = hit.transform.gameObject;
                    MonumentManager managerOfCurrentObject = currentMonument.transform.parent.GetComponent<MonumentManager>();

                    if (managerOfCurrentObject.Locked && !managerOfCurrentObject.Focused)
                    {
                        QuestionStatic.CurrentQuestions = managerOfCurrentObject.questions;
                        QuestionStatic.CurrentManager = currentMonument.transform.parent.gameObject;
                        managerOfCurrentObject.Locked = false;
                        this.UIPanelsManager.SetActiveCanvas(UIPanelsInCavas.QuestionsPanel);
                    }
                }

                if (hit.collider.gameObject.tag == "Clock")
                {
                    Debug.Log("Clock");
                    GameObject clock = hit.collider.gameObject;
                    this.TimerPanel.SetActive(true);
                    TimerText.text = clock.GetComponent<Clock>().smallTekst;
                }

             
              
            }
        }
    }
}
