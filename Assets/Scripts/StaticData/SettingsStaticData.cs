﻿using UnityEngine;

namespace Assets.Scripts.StaticData
{
    public static class SettingsStaticData
    {
        public static bool flashlightTurnedOn = PlayerPrefs.GetInt("flashlight", 0) == 1;
        public static bool isFXMuted = PlayerPrefs.GetInt("isFXMuted", 0) == 1;
        public static bool isMusicMuted = PlayerPrefs.GetInt("isMusicMuted", 0) == 1;
    }
}
