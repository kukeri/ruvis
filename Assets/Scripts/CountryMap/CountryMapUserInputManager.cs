﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using Assets.Scripts.UI.InGameUI.Panels;

public class CountryMapUserInputManager : MonoBehaviour
{
    public MapManager mapManager;
    public PanelsManager UIPanelsManager;
                    
    void Update()
    {
        CheckForHit();
    }

    private void CheckForHit()
    {
        if (Input.GetMouseButtonDown(0))
        {

            Vector2 positionOfHit = Input.mousePosition;

            Ray currentRaycast =
                this.GetComponent<Camera>().ScreenPointToRay(new Vector3(positionOfHit.x, positionOfHit.y, 0));

            RaycastHit hit;
            if (Physics.Raycast(currentRaycast, out hit))
            {
                Transform region = hit.transform;
                if (region.name.StartsWith("region") && !region.GetComponent<RegionManager>().locked.activeSelf)
                {
                    mapManager.SetOnFocus(region);
                    UIPanelsManager.SetActiveCanvas(UIPanelsInCavas.SelectedRegionPanel);
                }
            }
        }
    }
}
