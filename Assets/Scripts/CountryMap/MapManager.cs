﻿using UnityEngine;
using System.Collections;

public class MapManager : MonoBehaviour
{
    private const int TimeForScale = 25;

    public GameObject regionSelectedCanvas;
    public GameObject[] regionsAndMap;

    private bool hasFocusedRegion;
    private bool regionShouldBeFocused;

    private float deltaToAddForXScale;
    private float deltaToAddForYScale;
    private float deltaToAddForZScale;

    private float deltaToAddForXRepositioning;
    private float deltaToAddForYRepositioning;
    private float deltaToAddForZRepositioning;

    private Transform regionToFocus;
    private RegionManager _regionToFocusManager;
    private int currentScale;

    void Start()
    {
        currentScale = 0;
        regionShouldBeFocused = false;
        hasFocusedRegion = false;
    }

    void Update()
    {
        ScaleAndFocusSelectedRegion();
    }

    public void SetOnFocus(Transform transformOfGameObjectToFocus)
    {
        if (!hasFocusedRegion)
        {
            this.regionToFocus = transformOfGameObjectToFocus;
            this._regionToFocusManager = transformOfGameObjectToFocus.gameObject.GetComponent<RegionManager>();

            regionShouldBeFocused = true;
            deltaToAddForXScale = transformOfGameObjectToFocus.localScale.x / TimeForScale;
            deltaToAddForYScale = transformOfGameObjectToFocus.localScale.y / TimeForScale;
            deltaToAddForZScale = transformOfGameObjectToFocus.localScale.z / TimeForScale;

            deltaToAddForXRepositioning = transformOfGameObjectToFocus.localPosition.x / (TimeForScale*3);
            deltaToAddForYRepositioning = transformOfGameObjectToFocus.localPosition.y/ (TimeForScale * 3);
            deltaToAddForZRepositioning = transformOfGameObjectToFocus.localPosition.z / (TimeForScale * 3);


            foreach (GameObject gameobject in regionsAndMap)
            {
                if (gameobject.name != transformOfGameObjectToFocus.gameObject.name)
                {
                    gameobject.SetActive(false);
                }
            }

            hasFocusedRegion = true;
        }
    }

    public void SetOutOfFocus()
    {
        foreach (GameObject gameObject in regionsAndMap)
        {
            gameObject.SetActive(true);
        }

        regionShouldBeFocused = true;
        deltaToAddForXRepositioning *= -1;
        deltaToAddForYRepositioning *= -1;
        deltaToAddForZRepositioning *= -1;

        deltaToAddForXScale *= -1;
        deltaToAddForYScale *= -1;
        deltaToAddForZScale *= -1;

        hasFocusedRegion = false;
    }

    private void ScaleAndFocusSelectedRegion()
    {
        if (regionShouldBeFocused && currentScale < TimeForScale*3)
        {                   
            this.regionToFocus.localScale = new Vector3(regionToFocus.localScale.x + deltaToAddForXScale, regionToFocus.localScale.y + deltaToAddForYScale, regionToFocus.localScale.z + deltaToAddForZScale);
            this.regionToFocus.localPosition = new Vector3(regionToFocus.localPosition.x - deltaToAddForXRepositioning, regionToFocus.localPosition.y - deltaToAddForYRepositioning, regionToFocus.localPosition.z - deltaToAddForZRepositioning);
            currentScale++;
            return;
        }

        if (regionShouldBeFocused)
        {                                                                     

            _regionToFocusManager.ShowHideMonuments();
            regionShouldBeFocused = false;
        }

        currentScale = 0;
    }

}
